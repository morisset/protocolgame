// check if the game is in debug mode
function inDebugMode() {
    return localStorage.getItem(window.location.href + "_debug") === "true";
}

//get the line number where breakpoint is defined
function getBreakPoint() {
    return localStorage.getItem(window.location.href + "_breakpoint") * 1;
}

// create breakpoint symbol on the UI
function createBreakPoint() {
    var marker = document.createElement("div");
    marker.className = "breakpoint";
    marker.style.color = "#822";
    marker.innerHTML = ">";
    return marker;
}

//Add breakpoint listener to after page reload 
function addBreakPointListener(line) {
    var hasBreakPoint = $(line).find(".breakpoint").length;

    var breakpoints = $(".breakpoint");
    if (breakpoints.length) {
        breakpoints.remove();
        localStorage.removeItem(window.location.href + "_breakpoint");
    }

    if (!hasBreakPoint) {
        $(line).prepend(createBreakPoint());
        localStorage.setItem(window.location.href + "_breakpoint", $(line).data("step"));
    }
}

// to hightlight HTML element with other color and weight
var hightLight = function (selector, color, fontWeight) {
    $(selector).css("color", color).css("font-weight", fontWeight);
};

/**
 * 
 * add debugger features to console input
 * 
 * Usage:
 *   
 *   $("commandtextarea").debugger({
 *     stepper: "#stepper",
 *      allAnnotations: "#showAll"
 *   });
 *

 */

(function ($) {

    $.fn.debugger = function (options) {
        var opts = $.extend({}, $.fn.debugger.defaults, options);

        // show all the visualisation of specific step
        var showStep = function (showStep, showAnnotation) {
            hightLight("[data-step]:not(.lineselect)", "black", "normal");
            hightLight(".message[data-step][data-msg-status='t']", "green", "normal");
            hightLight("[data-step=" + showStep + "]", "red", "bold");

            $("[data-step]:not(.lineno)").each(function () {
                var step = $(this).data("step");

                // checking for missing data-step - testing purpose
                if (step == "undefined") {
                    console.log("data-step is undefined: " + $(this).text());
                }

                if (showStep > step) {
                    if ($(this).hasClass("message")) {
                        $(this).css("display", "list-item");
                    } else if ($(this).hasClass("annotation")) {
                        $(this).css("display", showAnnotation ? "inline" : "none");
                    } else {
                        $(this).css("display", "inline");
                    }
                } else if (showStep == step) {
                    $(this).css("display", "inline");
                } else {
                    $(this).css("display", "none");
                }

            });
        }

        // get check box value
        var showAll = function () {
            return $(opts.allAnnotations).is(":checked");
        };

        return this.each(function () {

            var stepper = $(opts.stepper);
            var lastLine = stepper.prop("max") * 1;

            // to create line number and ensure there is always enough line number for the commands entered
            $(this).linedtextarea({
                initialLines: lastLine + 1
            });

            //add breapoint after page reload
            var breakPoint = getBreakPoint();
            if (breakPoint > 0) {
                var breakLine = $(".lineno[data-step=" + breakPoint + "]");
                breakLine.first().prepend(createBreakPoint());
            }
            
            if (inDebugMode()) {

                //find error and highlight the line number
                var error = localStorage.getItem(window.location.href + "_errors");

                if (error != null) {
                    var line = lastLine = error.split(",")[0] * 1 + 1;
                    hightLight("[data-step=" + line + "]", "red", "bold");
                    $(".lineno[data-step=" + line + "]").addClass("lineselect");
                    stepper.val(line);
                }

                // hide or show previous annotations
                $(opts.allAnnotations).on("change", function () {
                    var step = stepper.val() > 0 ? stepper.val() : 0;
                    stepper.val(step);
                    showStep(step, $(this).is(":checked"));
                });

                // event listener for the stepper
                stepper.on("change keyup", function () {
                    var step = $(this).val() === "" ? -1 : $(this).val();
                    showStep(step, showAll());
                });

                // messages, annotations, knowledge acts as a shortcut to find the associated command
                $("[data-step]:not(.lineno)").on("click", function () {
                    var step = Math.ceil($(this).data("step"));
                    if (lastLine >= step) {
                        stepper.val(step);
                        showStep(step, showAll());
                    }
                });

                //trigger the breakpoint
                if (!error) {
                    var max = stepper.prop("max");
                    stepper.val((breakPoint > 0 && breakPoint <= max) ? breakPoint : max).trigger("change");
                }
            }
        });
    }

    //default options
    $.fn.debugger.defaults = {
        stepper: "#stepper",
        allAnnotations: "#showAll"
    };
})(jQuery);