describe("Command", function() {
    var command;
    var id = 1;
    var cmd = "new_session();"

    beforeEach(function() {
        netStack = new NetStack('messageList');
        agentA = new Agent(id,[],netStack);
        agentB = new Agent(id+1,[],netStack);
        agentS = new Agent(id+2,[],netStack);
        attacker = new Agent(id+3,[],netStack);
        command = new Command(netStack,attacker,[agentA,agentB,agentS])
    });

    it("constructor", function() {
        expect(command.step).toEqual(1);
        expect(command.agent_list.length).toEqual(3);
        expect(command.attacker.id).toEqual(id+3);
    });

    it("santitise str", function() {
        expect(command.santitise_commands("\r\n"+cmd)).toEqual(cmd);
    });

    it("store command without debugging", function() {
        var debug = false;
        command.store_cmds(cmd,debug);
        var storedStr= localStorage.getItem(window.location.href);
        var debugMode= localStorage.getItem(window.location.href+"_debug");
        var errors= localStorage.getItem(window.location.href+"_errors");
        expect(storedStr).toEqual(cmd);
        expect(debugMode).toEqual(debug+'');
        expect(errors).toEqual(null);
        localStorage.clear();
    });

});
