describe("Fact test", function() {
    var str = 'S';
    var step = 1;
    var key = 'K_0';
    var cipher =  '{' + str + '}' + key;   
    var fact;
  
    beforeEach(function() {
        fact = new Fact(str,step);
    });
  
    it("contructor", function() {
        console.log("Fact contructor testing");
        console.log("-------------------");
        expect(fact.str).toEqual(str);
        console.log("fact expect: "+ str + " - actual: "+ fact.str);
        expect(fact.step).toEqual(step);
        console.log("step expect: "+ step + " - actual: "+ fact.step);
        console.log("-------------------");
        
    });

    it("encrypt test method call only", function() {
        console.log("Testing Encrypt method call");
        expect(fact.encrypt(key)).toEqual(cipher);
        console.log("encrypt method expect: "+ cipher + " - actual: "+ fact.encrypt(key));
        console.log("-------------------");
    });

    it("decrypt test method call only", function() {
        console.log("Testing decrypt method call");
        var newStep = step+1; 
        fact = new Fact(cipher,newStep);
        expect(fact.step).toEqual(newStep);   
        expect(fact.decrypt(key)).toEqual(str);
        console.log("decrypt method expect: "+ str + " - actual: "+ fact.decrypt(key));
        console.log("-------------------");
    });
});