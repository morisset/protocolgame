describe("Protocol Game test", function () {
    var str = 'secret';
    var source = 'A';
    var destination = 'A';
    var key = 'K_0';
    var cipher = '{' + str + '}' + key;
    var msg = {source: source, destination: destination, content: str};

    it("isEncrypted normal", function () {
        expect(cipher.isEncrypted()).toEqual(true);
    });

    it("isEncrypted non cipher", function () {
        expect(str.isEncrypted()).toEqual(false);
    });

    it("encrypt normal", function () {
        expect(str.encrypt(key)).toEqual(cipher);
    });

    it("decrypt normal", function () {
        expect(cipher.decrypt(key)).toEqual(str);
    });

    it("decrypt wrong key", function () {
        expect(function () {
            cipher.decrypt("K_1");
        }).toThrow();
    });

    it("print message", function () {
        expect(print_message(msg)).toEqual(msg.source + ' -> ' + msg.destination + ": " + msg.content);
    });

    it("create message", function () {
        var str_msg = print_message(msg);
        var new_msg = create_message(str_msg);
        expect(new_msg.source).toEqual(source);
        expect(new_msg.destination).toEqual(destination);
        expect(new_msg.content).toEqual(str);
    });
});