describe("Agent", function() {
    var agent;
    var netStack;
    var str = 'S';
    var key = 'K_0';
    var cipher =  '{' + str + '}' + key;   
    var step = 1;
    var id = 1;

    beforeEach(function() {
        netStack = new NetStack('message');
        agent = new Agent(id,[],netStack);
        
    });

    it("init state", function() {
        expect(agent.state).toEqual('init');
        expect(agent.decryption_attempt).toEqual(0);
        expect(agent.facts.length).toEqual(0);
        expect(agent.msg_stack.length).toEqual(0);
    });

    describe("fact tests", function() {

        it("know fact", function() {
            agent.learns(str,1)
            var fact = agent.knows(str);
            expect(fact.str).toEqual(str);
        });

        it("learn fact", function() {     
            agent.learns(str,1)
            expect(agent.facts.length).toEqual(1);
            expect(agent.facts[0].str).toEqual(str);
        });

        it("forget fact", function() {
            agent.learns(str,1)
            agent.forgets(str);
            expect(agent.facts.length).toEqual(0);
        });
    });

    describe("message tests", function() {
 
        it("send message", function() {       
            agent.learns(str,step);
            const msg = {source: agent.id , destination: 'B', content: 'S'};
            agent.sendMessage(msg, step+1);
            
            expect(netStack.stack[0].step).toEqual(2);
            expect(netStack.stack[0].msg.destination).toEqual('B');
            expect(netStack.stack[0].msg.content).toEqual('S');
        });

    });

    describe("encrypt tests", function() {

        it("encrypt normal", function() {
            agent.learns(str,step)
            agent.learns(key,step+1)
            expect(agent.encrypt(str, key, step+2)).toEqual(cipher);
        });

        it("encrypt unknown key", function() {
            agent.learns(str,step);
            
            expect(function() {
                agent.encrypt(str, key, step+1);
            }).toThrow();
        });

        it("encrypt unknown string", function() {
            agent.learns(key,step);
            expect(function() {
                agent.encrypt(str, key, step+1);
            }).toThrow();
        });
    });
    describe("decrypt tests", function() {
        it("decrypt normal", function() {
            agent.learns(cipher,step);
            agent.learns(key,step+1);
            expect(agent.decrypt(cipher, key, step+2)).toEqual(str);
        });

        it("decrypt not cipher message", function() {
            agent.learns(str,step);
            agent.learns(key,step+1);
            expect(function() {
                agent.decrypt(str, key, step+2);
            }).toThrow();
        });

        it("decrypt unknown key", function() {
            agent.learns(cipher,step);
            
            expect(function() {
                agent.decrypt(cipher, key, step+1);
            }).toThrow();
        });

        it("decrypt unknown string", function() {
            agent.learns(key,step);

            expect(function() {
                agent.decrypt(cipher, key, step+1);
            }).toThrow();
        });
    });

});
