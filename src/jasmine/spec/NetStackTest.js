describe("Net Stack", function() {
    var netStack;
    var id = 1;
    var agent;

    beforeEach(function() {
        netStack = new NetStack('messageList');
        agent = new Agent(id,[],netStack);
    });

    it("constructor", function() {
        expect(netStack.stack).toEqual({});
        expect(netStack.attacker).toEqual(null);
        expect(netStack.agent_list.length).toEqual(0);
        expect(netStack.pastKeys.length).toEqual(0);
    });

    it("register 1 agent", function() {
        netStack.registerAgents([agent]);
        expect(netStack.agent_list.length).toEqual(1);
        expect(netStack.agent_list[0].id).toEqual(agent.id);
    });

    it("register multiple agents", function() {
        agent2 = new Agent(id+1,[],netStack);
        agent3 = new Agent(id+2,[],netStack);
        netStack.registerAgents([agent,agent2,agent3]);
        expect(netStack.agent_list.length).toEqual(3);
        expect(netStack.agent_list[0].id).toEqual(agent.id);
        expect(netStack.agent_list[1].id).toEqual(agent2.id);
        expect(netStack.agent_list[2].id).toEqual(agent3.id);
    });

    it("register attacker", function() {
        netStack.registerAttacker(agent);
        expect(netStack.attacker).not.toEqual(null);
        expect(netStack.attacker.id).toEqual(agent.id);
    });

    describe("test messages", function() {
        var step = 1;
        var str = 'S';
        var msg = {source:id, destination:id+1, content: str};
        var status = "w";

        beforeEach(function() {
            netStack = new NetStack('messageList');
            agent = new Agent(id,[],netStack);
            agent2 = new Agent(id+1,[],netStack);
            agent3 = new Agent(id+2,[],netStack);
            netStack.registerAgents([agent,agent2,agent3]);
        });

        it("message constructor", function() {
            var message = new Message(msg, step, 'w');
            expect(message.msg.content).toEqual(str);
            expect(message.msg.source).toEqual(id);
            expect(message.msg.destination).toEqual(id+1);
            expect(message.step).toEqual(step);
            expect(message.status).toEqual('w');
        });


        it("add simple message (no HTML element)", function() {
            var msgId = netStack.getFreshId();
            netStack.addMessage(str, step, 'w');
            var message = netStack.stack[msgId];
            expect(message.msg).toEqual(str);
            expect(message.step).toEqual(step);
            expect(message.status).toEqual(status);
        });

        it("block message waiting", function() {
            var msgId = netStack.getFreshId();
            netStack.addMessage(str, step, 'w');
            netStack.blockMessage(msgId)
            expect(netStack.stack[msgId].status).toEqual('b');
        });

        it("block message waiting intercepted", function() {
            var msgId = netStack.getFreshId();
            netStack.addMessage(str, step, 'wi');
            netStack.blockMessage(msgId)
            expect(netStack.stack[msgId].status).toEqual('ib');
        });

        it("block message trasnmited", function() {
            var msgId = netStack.getFreshId();
            netStack.addMessage(str, step, 't');      
            expect(function() {
                netStack.blockMessage(msgId)
            }).toThrow();
        });

        it("change status", function() {
            var msgId = netStack.getFreshId();
            netStack.addMessage(str, step, 'w');
            netStack.changeStatus(msgId,'wi')
            expect(netStack.stack[msgId].status).toEqual('wi');
        });

        it("clear stack", function() {
            netStack.addMessage(str, step, 'w');
            netStack.clearStack(step)
            expect(netStack.stack).toEqual({});
        });
    });
});