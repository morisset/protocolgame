let agent = require('../src/Agent.js');
let netstack = require('../src/NetStack.js')
let command =  require('../src/Command.js')

class Sender extends agent.Agent {

    constructor(id, facts, network) {
        super(id, facts, network);
        this.session = 0;
        this.state = 0;
    }

    // Sender has the following states
    // init: initial state, ready to send message 1
    // (in case of multiple sessions, message 3 from previous session
    // has been sent)
    // waiting_2: message 1 sent, waiting for message 2.

    process (msg, step) {
        switch (this.state) {
            case 0:
                this.init(step); break;
            case 1:
                this.sendEncryptedSecret(msg, step); break;
            default:
                throw 'Unknown state for agent ' + this.id;
        }
    }

    getSessionSecret (step) {
        var secret = 's_' + this.session;
        this.learns(secret, step)
        return secret;
    }

    sendEncryptedSecret(msg, step) {
        var facts = msg.content.getFacts();
        if (facts.length != 1) {
            console.log('Expecting exactly one argument, received: ' + facts.length);
            return;
        }

        var k = facts[0];
        this.learns(k, step);

        var new_msg = {source:this.id, destination:'B',
            content:this.encrypt(this.getSessionSecret(step), k, step)};
        this.sendMessage(new_msg, step);
        this.state = 0;

    }

    init (step) {
        var msg = {source:this.id, destination:'B', content:'hello'};
        this.sendMessage(msg, step);
        this.session++;
        this.state = 1;
    }
}

class Receiver extends agent.Agent {
    constructor(id, facts, network) {
        super(id, facts, network);
        this.session = 0;
        this.state = 0;
        this.key = null;
    }

    process(msg, step) {
        switch (this.state) {
            case 0:
                this.sendKey(msg, step); break;
            case 1:
                this.decryptMessage(msg, step); break;
            default:
                throw 'Unknown state for agent ' + this.id;

        }


    }

    getFreshKey(step) {
        var k = 'K_' + this.session;
        this.learns(k, step)
        this.key = k;
        return k;
    }


    sendKey(msg, step) {
        var facts = msg.content.getFacts();
        if (facts.length != 1) {
            console.log('Expecting exactly one argument, received: ' + facts.length);
            return;
        }

        var hello = facts[0];
        if (hello != 'hello') {
            console.log('Expecting hello, received: ' + hello);
            return;
        }
        this.learns(hello, step);
        var new_msg = {source:this.id, destination:'A',
            content:this.getFreshKey(step)};
        this.sendMessage(new_msg, step);
        this.state = 1;
    }

    decryptMessage(msg, step) {
        var facts = msg.content.getFacts();
        if (facts.length != 1) {
            console.log('Expecting exactly one argument, received: ' + facts.length);
            return;
        }

        this.learns(facts[0]);
        this.decrypt(facts[0], this.key, step);
        this.step = 0;
    }

}

class Attacker extends agent.Agent {
    process (msg) {
        this.learns(msg.content.getFacts()[0]);
    }
}

var s1 = new netstack.NetStack('message_list1', cl=true);
var a1 = new Sender('A', [], s1);
var b1 = new Receiver('B', [], s1);
var e1 = new Attacker('E', [], s1);
s1.registerAgents([a1, b1, e1]);
s1.registerAttacker(e1);
var c1 = new command.Command(s1, e1, [a1, b1, e1]);
a1.learns('s', 0);
a1.learns('hello', 0);
b1.learns('K', 0);
e1.learns('fake', 0);

setCurrentStep(0);
console.log(agent)

